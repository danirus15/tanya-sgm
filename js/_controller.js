$( document ).ready(function() {
	var elem = $("#chars");
	$("#text").limiter(140, elem);

  var elem = $("#chars2");
  $("#text2").limiter(140, elem);
});


  function findValue(li) {
    if( li == null ) return alert("No match!");

    // if coming from an AJAX call, let's use the CityId as the value
    if( !!li.extra ) var sValue = li.extra[0];

    // otherwise, let's just display the value in the text box
    else var sValue = li.selectValue;

    //alert("The value you selected was: " + sValue);
  }

  function selectItem(li) {
        findValue(li);
  }

  function formatItem(row) {
        return row[0];
  }
  
  
$("#text").autocomplete(
  "autocomplete.php",
  {
        delay:10,
        minChars:2,
        matchSubset:1,
        matchContains:1,
        cacheLength:10,
        onItemSelect:selectItem,
        onFindValue:findValue,
        formatItem:formatItem,
        autoFill:true
    }
);

$("#text2").autocomplete(
  "autocomplete.php",
  {
        delay:10,
        minChars:2,
        matchSubset:1,
        matchContains:1,
        cacheLength:10,
        onItemSelect:selectItem,
        onFindValue:findValue,
        formatItem:formatItem,
        autoFill:true
    }
);


$(function() {
  $('#header_slide').carouFredSel({
    responsive: true,
    items: {
      visible: 1,
      width: 900,
      height: 500
    },
    scroll: {
      duration: 1000,
      timeoutDuration: 5000,
      fx: 'crossfade'
    }
  });
});


$(function () {
  $('.item').css('position','absolute').css('left','0').css('top','0');
  var $container = $('.masonry').masonry({ columnWidth: 5 });
  $(".item-content" ).click(function() {
    if($(this).parent('.item').hasClass('expanded')){
      $container.on('click', '.close', function () {
        $('.item').removeClass('expanded');
        $container.masonry();
      });
    }
    else{
      $container.on('click', '.area_list', function () {
        $('.item').removeClass('expanded');
        $('.item').removeClass('no-expanded');

        $(this).parent().parent('.item').addClass('expanded');
        $container.masonry();
      });
    }
  });

   $(".filter_cate a" ).click(function() {
    var src  = $(this).attr("alt");
    $(".filter_cate a").removeClass('selected');
    $(this).addClass('selected');
    $('.item').hide();
    $('.' + src).show();
    $container.masonry();
    //alert(src);
  });
});

$(function () {
  $('.item-faq').css('position','absolute').css('left','0').css('top','0');
  var $container = $('.masonry2').masonry({ columnWidth: 5 });
  
  $(".item-content" ).click(function() {
    if($(this).parent('.item-faq').hasClass('expanded2')){
      $container.on('click', '.expanded2', function () {
        $('.item-faq').removeClass('expanded2');
        $container.masonry();
      });
    }
    else{
      $container.on('click', '.area_list', function () {
        $('.item-faq').removeClass('expanded2');
        $('.item-faq').removeClass('no-expanded');
        $('.item-faq').removeClass('mulai');
        $(this).parent().parent('.item-faq').addClass('expanded2');
        $container.masonry();
      });
    }
  });
});


setTimeout(function() {
    $('.item').removeClass('mulai');
    $('.item-faq').removeClass('mulai');
}, 1000);

$(".share_share").click(function (){
  $('.sharefull').fadeOut();
  $(this).parent().parent().find('.sharefull').fadeIn(200);
});
$(".close_share").click(function (){
  $('.sharefull').fadeOut();
});



/*S:MODALBOX*/
if( $('div.pop_box').attr('id') == 'pop_box_now'){
  $("body").css('overflow','hidden');
}
else {
  $("body").css('overflow','scroll');
}

$(".box_modal").click(function (){
  if( $('div').attr('id') == 'pop_box_now'){}
  else{ 
    $(this).removeAttr('href');
    var src  = $(this).attr("alt");
    size   = src.split('|');
        url      = size[0],
        width    = '100%',
        height   = '100%'

    $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('position','fixed').css('top','0').css('left','0');
    $("body").css('overflow','hidden');
  }
});
$(".box_modal2").click(function (){
  $("#pop_box2").show();
  $("body").css('overflow','hidden');
});
$(".box_modal3").click(function (){
  $("#pop_box3").show();
  $("#pop_box2").hide();
  $("body").css('overflow','hidden');
});
$(".box_modal4").click(function (){
  $("#pop_box4").show();
  $("#pop_box3").hide();
  $("#pop_box2").hide();
  $("body").css('overflow','hidden');
});

function closepop() { 
  $("#pop_box_now").remove();
  $("#pop_box2").hide();
  $("#pop_box3").hide();
  $("body").css('overflow','scroll');
};
$(".close_box").click(function (){
  closepop();
});
$(".close_box_in").click(function (){
  parent.closepop();
});
/*S:MODALBOX*/


$("#tab1").idTabs(function(id,list,set){ 
    $("a",set).removeClass("selected") 
    .filter("[href='"+id+"']",set).addClass("selected"); 
    for(i in list) 
      $(list[i]).hide(); 
    $(id).fadeIn(); 
    return false; 
  });


