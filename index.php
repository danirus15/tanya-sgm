<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<?php include "includes/head.php";?>

<body>
<?php include "includes/header.php";?>
<div class="container2">
	<div class="filter" id="filter">
		<div class="fil1 filter_cate">
			<a href="#filter" alt="video"><img src="img/sort_video.png" alt=""></a>
			<a href="#filter" alt="gambar"><img src="img/sort_pic.png" alt=""></a>
			<a href="#filter" alt="texting"><img src="img/sort_text.png" alt=""></a>
		</div>
		<div class="fil2">
			<a><h5>Sejarah & Komitmen</h5></a>
			<a class="selected"><h5>bahan baku</h5></a>
			<a><h5>Jaminan kualitas</h5></a>
			<a><h5>Nutrisi & manfaat</h5></a>
			<a><h5>produk & penggunaan</h5></a>
			<a><h5>inovasi</h5></a>
		</div>
		<div class="fil3">
			<a href="#" class="selected"><h5>Most viewed</h5></a> 
			<span>&middot;</span>
			<a href="#"><h5>Most Recent</h5></a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container">

	<div class="masonry">
	  <div class="item mulai gambar">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	         <h2> Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="pic">
	          <img src="img/img1.jpg" alt="gambar">
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt="user"></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="clearfix"></div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	   
	  </div>
	  <div class="item mulai texting">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt="user"></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label2.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	       <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>

	  <div class="item mulai video">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="pic pic_video">
	          <img src="img/img3.jpg" alt="">
	          <img src="img/ico_video.png" alt="" class="video_ico">
	        </div>
	        <div class="videos">
	          <iframe width="560" height="315" src="//www.youtube.com/embed/6Lz5T1IYAqg" frameborder="0" allowfullscreen></iframe>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label3.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai gambar">
	    <div class="item-content">
	      <div class="tnya">
	        <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	      </div>
	      <div class="pic">
	        <img src="img/img2.jpg" alt="">
	      </div>
	      <div class="clearfix"></div>
	      <div class="user">
	        <div class="img"><img src="img/user1.jpg" alt=""></div>
	        <div class="name">
	          Jeanny
	          <span>Jakarta</span>
	        </div>
	        <div class="clearfix"></div>
	      </div>
	      <img src="img/label4.png" alt="label" class="label_post">

	      <div class="clearfix"></div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai gambar">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="pic">
	          <img src="img/img1.jpg" alt="">
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label5.png" alt="label" class="label_post">
	      </div>
	      <div class="clearfix"></div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	   
	  </div>
	  <div class="item mulai texting">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <!-- <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="">
	            123
	          </span>
	          <span>
	            <img src="img/ico_share.png" alt="">
	            249
	          </span>
	        </div> -->
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label6.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai gambar">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	         <h2> Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="pic">
	          <img src="img/img1.jpg" alt="">
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">
	      </div>
	      <div class="clearfix"></div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	   
	  </div>
	  <div class="item mulai texting">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>

	  <div class="item mulai video">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="pic pic_video">
	          <img src="img/img3.jpg" alt="">
	          <img src="img/ico_video.png" alt="" class="video_ico">
	        </div>
	        <div class="videos">
	          <iframe width="560" height="315" src="//www.youtube.com/embed/6Lz5T1IYAqg" frameborder="0" allowfullscreen></iframe>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai gambar">
	    <div class="item-content">
	      <div class="tnya">
	        <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	      </div>
	      <div class="pic">
	        <img src="img/img2.jpg" alt="">
	      </div>
	      <div class="clearfix"></div>
	      <div class="user">
	        <div class="img"><img src="img/user1.jpg" alt=""></div>
	        <div class="name">
	          Jeanny
	          <span>Jakarta</span>
	        </div>
	        <div class="clearfix"></div>
	      </div>
	      <img src="img/label1.png" alt="label" class="label_post">

	      <div class="clearfix"></div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai gambar">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="pic">
	          <img src="img/img1.jpg" alt="">
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">
	      </div>
	      <div class="clearfix"></div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	   
	  </div>
	  <div class="item mulai texting">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai video">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="item mulai video">
	    <div class="item-content">
	      <div class="area_list">
	        <div class="tnya">
	          <h2>Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?</h2>
	        </div>
	        <div class="clearfix"></div>
	        <div class="user">
	          <div class="img"><img src="img/user1.jpg" alt=""></div>
	          <div class="name">
	            Jeanny
	            <span>Jakarta</span>
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <img src="img/label1.png" alt="label" class="label_post">

	        <div class="clearfix"></div>
	      </div>
	      <div class="share">
	          <span>
	            <img src="img/ico_love.png" alt="share love">
	            123
	          </span>
	          <span class="share_share">
	            <img src="img/ico_share.png" alt=" share">
	            249
	          </span>
          </div>
          <div class="sharefull">
          	<span class="close_share"><img src="img/btn_close.png" alt=""></span>
          	<br><br><br>
          	Share<br>
          	<a href="#"><img src="img/ico_tw.png" alt="twitter"></a>
          	<a href="#"><img src="img/ico_fb.png" alt="facebook"></a>
          	<a href="#"><img src="img/ico_love.png" alt="love"></a>
          </div>
	      <div class="jawab_box">
	        <div class="jawab">
	          <img src="img/btn_close2.png" alt="" class="close">
	          <div class="jwb_label">Jawaban kami</div>
	          <h3>Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. </h3>
	          <div class="date">7 Januari 2015</div>
	        </div>
	        <div class="clearfix"></div>
	        <div class="share_box2">
	          <a href="#"><img src="img/share_tw.png" alt="twitter"></a>
	          <a href="#"><img src="img/share_fb.png" alt="facebook"></a>
	          <a href="#"><img src="img/share_like_1.png" alt="like"></a>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div align="center" class="pd20">
		<img src="img/circle-loading.gif" alt="" height="50">
	</div>
</div>
<div class="pop_box" id="pop_box2"><?php include "box_tanya.php";?></div>
<div class="pop_box" id="pop_box3"><?php include "box_form.php";?></div>
<div class="pop_box" id="pop_box4"><?php include "box_konfirmasi.php";?></div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
	
</body>
</html>