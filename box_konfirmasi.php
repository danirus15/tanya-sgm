<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/favicon.png"/> 
<title>Tanya SGM</title>
<link href="css/fonts.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/frame.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/pop.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link rel="shortcut icon" href="images/favicon.png">
</head>
<body>
	<div class="pop_container">
		<a><img src="img/logo_pop.jpg" alt="" class="logo_pop"></a>
		<a href="#" class="close_pop close_box_in"><img src="img/btn_close.png" alt=""></a>
		<h1 class="jdl">
			Terima kasih<br>
			telah mengajukan pertanyaan<br>
			kepada kamu
		</h1>
		<div class="list_1">
			<div class="no">1</div>
			<div class="text">
				Setiap pertanyaan yang kami terima, akan kami proses terlebih dahulu 
				untuk mengecek kesesuaikan pertanyaan dengan konteks tujuan website ini
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="list_1">
			<div class="no">2</div>
			<div class="text">
				Silahkan tunggu email notifikasi dari kami untuk memastikan bahwa pertanyaan anda telah kami proses
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="list_1">
			<div class="no">3</div>
			<div class="text">
				Untuk setiap pertanyaan, kami membutuhkan waktu mulai dari hitungan jam hingga hitungan hari untuk memastikan kualitas jawaban kami dapat menjawab pertanyaan anda dengan tepat
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="list_1">
			<div class="no">4</div>
			<div class="text">
				Apabila ada yang belum dimengerti silahkan hubungi layanan bebas pulsa kami di 0 800 0 360 360 
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<br><br>
		<div align="center">
			<a href="index.php" class="btn_white" target="_parent">Tanya jawab lainnya</a>
		</div>
		<br><br>
	</div>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/pop.controller.js"></script>
</html>