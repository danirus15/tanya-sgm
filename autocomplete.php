<?php

$q = strtolower($_GET["q"]);
if (!$q) return;
$items = array(
	"Apakah bila ini dan itu terjadi bilamana?",
	"Apakah bahan dari itu adalah bahan ini?",
	"Apakah susu dari kedelai dan sapi?",
	"Apakah coba-coba ini itu?",
	"Apakah susu ini juga sama?",
	"Apakah penting nya bila itu dan ini di dalam susu?",
);

foreach ($items as $key) {
	if (strpos(strtolower($key), $q) !== false) {
		echo "$key\n";
	}
}

?>