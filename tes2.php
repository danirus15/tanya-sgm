
<!DOCTYPE html><html class=''>
<head><meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="canonical" href="http://codepen.io/desandro/pen/daKBo" />


<style class="cp-pen-styles">* {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}

@font-face { font-family: eggff; src: url('fonts/Egg__s_Handwriting_by_callegg.ttf'); } 
@font-face { font-family: geff; src: url('fonts/ufonts.com_ge-inspira.ttf'); } 
body {
  background: #fff6f6;
  padding: 0;
  margin: 0;
  font-family: 'geff', helvetica, arial;
  font-style: 13px;
  line-height: 130%;
}
.clearfix {
  clear: both;
}

.masonry {
  width: 1050px;
}

.masonry .item {
  float: left;
}

/* item is invisible, but used for layout */
.item {
  width: 350px;
  border: none;
  background: transparent;
  position: relative;

}

/* item-content is visible, and transitions size */
.item-content {
  margin: 5px;
  background: #fff;
  -webkit-transition: width 0.4s, height 0.4s;
     -moz-transition: width 0.4s, height 0.4s;
       -o-transition: width 0.4s, height 0.4s;
          transition: width 0.4s, height 0.4s;
  color: #000;
  border: 1px solid #ccc;
  position: relative;
}
.item-content .tnya {
  background-color: #ed282f;
  padding: 10px;
  padding-bottom: 30px;
  font-size: 15px;
  color: #fff;
}
.item-content .pic {
  width: 100%;
  height: 200px;
  overflow: hidden;
  position: relative;
  margin-top: -20px;
}
.item-content .pic img {
  width: 100%;
  height: auto;
}
.item-content .pic .video_ico {
  position: absolute;
  width: 50px;
  height: 50px;
  z-index: 1;
  top: 50%;
  left: 50%;
  margin-left: -25px;
  margin-top: -25px;
}
.item-content .share {
  text-align: center;
  margin-left: 120px;
  color: #565656;
  font-size: 12px;
  position: relative;
  margin-top: -25px;
}
.item-content .share img {
  display: block;
  height: 50px;
}
.item-content .share span {
  float: left;
  width: 41px;
  margin-right: 20px;
}
.item-content .user {
  padding: 10px;
  position: relative;
}
.item-content .user  .img {
  width: 60px;
  height: 60px;
  text-align: center;
  float: left;
  margin-right: 10px;
}
.item-content .user  .img img {
  height: 100%;
}
.item-content .user .name {
  position: absolute;
  left: 80px;
  bottom: 10px;
  color: #b14242;
  font-size: 15px;
  line-height: 120%;
}
.item-content .user .name span {
  color: #000;
  font-size: 13px;
  display: block;
}
.item-content .label_post {
  position: absolute;
  right: 15px;
  bottom: 15px;
  z-index: 1;
}
.item-content .video {
  display: none;
}


.mulai {
-webkit-transition: all 0.5s ease-in-out;
  -moz-transition: all 0.5s ease-in-out;
  -o-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
}
.item:hover .item-content {
  border-color: #ed282f;
  cursor: pointer;
}
.isi2 {
  display: none;
}
/* both item and item content change size */
.expanded{
  width: 700px;
}
.expanded .isi2,
.expanded .close {
  display: block;
}

.expanded {
  z-index: 2;
}
.expanded .tnya {
  padding-bottom: 10px;
}
.expanded .pic {
  margin-top: 0;
}
.expanded .video {
  display: block;
}
.expanded .video iframe {
  width: 100%;
  height: 400px;
}
.expanded .pic_video {
  display: none;
}
.close {
  cursor: pointer;
  display: inline-block;
  padding: 3px;
  display: none; ;
  height: 25px;
  z-index: 3;
  position: absolute;
  right: 0;
  top: 0;
}
.close:hover {
  background: #fbdadc;
}

.jawab_box {
  padding: 10px;
  display: none;
}
.expanded .share{
  display: none;
}
.expanded .item-content .jawab_box {
  display: block;
}
.expanded .pic {
  height: auto;
}
.jawab_box .jawab {
  background: #fef0f1;
  padding: 30px 15px 10px;
  position: relative;
  font-size: 13px;
  line-height: 130%;
}
.jawab_box .jwb_label{
  position: absolute;
  left: 0;
  top: 0;
  background: #ed282f;
  padding: 3px 20px;
  color: #fff;
}
.jawab_box .jawab .date {
  padding-top: 20px;
  color: #c11117;
  font-size: 12px;
}
.share_box2 {
  margin-top: 10px;
}
.share_box2 a:hover {
  opacity: 0.7;
}




</style></head><body>
<!-- <div class="masonry">
  <div class="item mulai">
    <div class="item-content">
      <div class="area_list">
        <div class="tnya">
          Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
        </div>
        <div class="pic">
          <img src="img/img1.jpg" alt="">
        </div>
        <div class="share">
          <span>
            <img src="img/ico_love.png" alt="">
            123
          </span>
          <span>
            <img src="img/ico_share.png" alt="">
            249
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="user">
          <div class="img"><img src="img/user1.jpg" alt=""></div>
          <div class="name">
            Jeanny
            <span>Jakarta</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <img src="img/label1.png" alt="" class="label_post">
      </div>
      <div class="clearfix"></div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
   
  </div>
  <div class="item mulai">
    <div class="item-content">
      <div class="area_list">
        <div class="tnya">
          Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
        </div>
        <div class="share">
          <span>
            <img src="img/ico_love.png" alt="">
            123
          </span>
          <span>
            <img src="img/ico_share.png" alt="">
            249
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="user">
          <div class="img"><img src="img/user1.jpg" alt=""></div>
          <div class="name">
            Jeanny
            <span>Jakarta</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <img src="img/label1.png" alt="" class="label_post">

        <div class="clearfix"></div>
      </div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>

  <div class="item mulai">
    <div class="item-content">
      <div class="area_list">
        <div class="tnya">
          Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
        </div>
        <div class="pic pic_video">
          <img src="img/img3.jpg" alt="">
          <img src="img/ico_video.png" alt="" class="video_ico">
        </div>
        <div class="video">
          <iframe width="560" height="315" src="//www.youtube.com/embed/6Lz5T1IYAqg" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="share">
          <span>
            <img src="img/ico_love.png" alt="">
            123
          </span>
          <span>
            <img src="img/ico_share.png" alt="">
            249
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="user">
          <div class="img"><img src="img/user1.jpg" alt=""></div>
          <div class="name">
            Jeanny
            <span>Jakarta</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <img src="img/label1.png" alt="" class="label_post">

        <div class="clearfix"></div>
      </div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="item mulai">
    <div class="item-content">
      <div class="tnya">
        Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
      </div>
      <div class="pic">
        <img src="img/img2.jpg" alt="">
      </div>
      <div class="share">
        <span>
          <img src="img/ico_love.png" alt="">
          123
        </span>
        <span>
          <img src="img/ico_share.png" alt="">
          249
        </span>
      </div>
      <div class="clearfix"></div>
      <div class="user">
        <div class="img"><img src="img/user1.jpg" alt=""></div>
        <div class="name">
          Jeanny
          <span>Jakarta</span>
        </div>
        <div class="clearfix"></div>
      </div>
      <img src="img/label1.png" alt="" class="label_post">

      <div class="clearfix"></div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="item mulai">
    <div class="item-content">
      <div class="area_list">
        <div class="tnya">
          Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
        </div>
        <div class="pic">
          <img src="img/img1.jpg" alt="">
        </div>
        <div class="share">
          <span>
            <img src="img/ico_love.png" alt="">
            123
          </span>
          <span>
            <img src="img/ico_share.png" alt="">
            249
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="user">
          <div class="img"><img src="img/user1.jpg" alt=""></div>
          <div class="name">
            Jeanny
            <span>Jakarta</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <img src="img/label1.png" alt="" class="label_post">
      </div>
      <div class="clearfix"></div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
   
  </div>
  <div class="item mulai">
    <div class="item-content">
      <div class="area_list">
        <div class="tnya">
          Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
        </div>
        <div class="share">
          <span>
            <img src="img/ico_love.png" alt="">
            123
          </span>
          <span>
            <img src="img/ico_share.png" alt="">
            249
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="user">
          <div class="img"><img src="img/user1.jpg" alt=""></div>
          <div class="name">
            Jeanny
            <span>Jakarta</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <img src="img/label1.png" alt="" class="label_post">

        <div class="clearfix"></div>
      </div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>

  <div class="item mulai">
    <div class="item-content">
      <div class="area_list">
        <div class="tnya">
          Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
        </div>
        <div class="pic">
          <img src="img/img3.jpg" alt="">
        </div>
        <div class="share">
          <span>
            <img src="img/ico_love.png" alt="">
            123
          </span>
          <span>
            <img src="img/ico_share.png" alt="">
            249
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="user">
          <div class="img"><img src="img/user1.jpg" alt=""></div>
          <div class="name">
            Jeanny
            <span>Jakarta</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <img src="img/label1.png" alt="" class="label_post">

        <div class="clearfix"></div>
      </div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>
  <div class="item mulai">
    <div class="item-content">
      <div class="tnya">
        Donec justo quam, laoreet ut, fermentum at, bla ndit vitae ligula? Vestibulum diam?
      </div>
      <div class="pic">
        <img src="img/img2.jpg" alt="">
      </div>
      <div class="share">
        <span>
          <img src="img/ico_love.png" alt="">
          123
        </span>
        <span>
          <img src="img/ico_share.png" alt="">
          249
        </span>
      </div>
      <div class="clearfix"></div>
      <div class="user">
        <div class="img"><img src="img/user1.jpg" alt=""></div>
        <div class="name">
          Jeanny
          <span>Jakarta</span>
        </div>
        <div class="clearfix"></div>
      </div>
      <img src="img/label1.png" alt="" class="label_post">

      <div class="clearfix"></div>
      <div class="jawab_box">
        <div class="jawab">
          <img src="img/btn_close2.png" alt="" class="close">
          <div class="jwb_label">Jawaban kami</div>
          Donec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. VestibulumDonec justo quam, laoreet ut, fermentum at, blandit vitae, ligula. Vestibulum diam. Etiam ut velit nec lacus consectetuer sodales. Integer accumsan. Maecenas eleifend vestibulum libero. 
          <div class="date">7 Januari 2015</div>
        </div>
        <div class="clearfix"></div>
        <div class="share_box2">
          <a href="#"><img src="img/share_tw.png" alt=""></a>
          <a href="#"><img src="img/share_fb.png" alt=""></a>
          <a href="#"><img src="img/share_like.png" alt=""></a>
        </div>
      </div>
    </div>
  </div>

</div> -->

<script src='js/jquery.min.js'></script>
<!-- <script src="//code.jquery.com/jquery-1.10.2.min.js"></script> -->
<!-- <script src='js/masonry.pkgd.js'></script> -->
<script src="//code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>

<script>


/*$(function () {
  $('.item').css('position','absolute').css('left','0').css('top','0');
  var $container = $('.masonry').masonry({ columnWidth: 350 });
  
  $(".item-content" ).click(function() {
    if($(this).parent('.item').hasClass('expanded')){
      $container.on('click', '.close', function () {
        $('.item').removeClass('expanded');
        $container.masonry();
      });
    }
    else{
      $container.on('click', '.area_list', function () {
        $('.item').removeClass('expanded');
        $('.item').removeClass('no-expanded');
        $('.item').removeClass('mulai');
        $(this).parent().parent('.item').addClass('expanded');
        $container.masonry();
      });
    }
  });
});*/

function changeOrientation(){
switch(window.orientation) {
case 0: // portrait, home bottom
case 180: // portrait, home top
 alert("portrait H: "+$(window).height()+" W: "+$(window).width());       
 break;
          case -90: // landscape, home left
          case 90: // landscape, home right
        alert("landscape H: "+$(window).height()+" W: "+$(window).width());
            break;
        }
    }

 window.onorientationchange = function() { 
            //Need at least 800 milliseconds
            setTimeout(changeOrientation, 1000);
        }
        
/*if(window.innerHeight > window.innerWidth){
    alert("Please use Landscape!");
}*/

/*$(window).on("orientationchange",function(){
  alert("The orientation has changed!");
});*/

/*$( window ).on( "orientationchange", function( event ) {
  $( "#orientation" ).text( "This device is in " + event.orientation + " mode!" );
  alert(event.orientation);
});
 
$( window ).orientationchange();*/
</script>
</body></html>